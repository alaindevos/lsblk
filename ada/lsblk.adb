--  USES ADA-UTIL , compiles like samples in ADA-UTIL
--  cfr samples.gpr in ADA-UTIL

with Ada.Containers.Indefinite_Hashed_Maps;
with Ada.Strings.Hash;
with Ada.Strings.Unbounded;  use Ada.Strings.Unbounded;
with Ada.Text_IO;            use Ada.Text_IO;
with Ada.Characters.Latin_1; use Ada.Characters;
with Util.Processes;
with Util.Streams.Pipes;
with Util.Streams.Buffered;
with GNAT.Regpat;
with GNAT.String_Split;      use GNAT.String_Split;
use Ada.Containers;
use GNAT;

procedure lsblk is

   package Pat renames GNAT.Regpat;

   function TUS (myin : String) return Unbounded_String is
   begin return To_Unbounded_String (myin); end TUS;

   function TS (myin : Unbounded_String) return String is
   begin return To_String (myin); end TS;

   procedure Put_Line (myin : Unbounded_String) is
   begin Put_Line (TS (myin)); end Put_Line;

   function Padme (astring : in Unbounded_String; myin : Integer)
     return Unbounded_String is
   begin
      return ("|" & astring & ((myin - Length (astring)) * " "));
   end Padme;

   package SS_Hashed_Maps is new
     Ada.Containers.Indefinite_Hashed_Maps
       (Key_Type        => String,
        Element_Type    => String,
        Hash            => Ada.Strings.Hash,
        Equivalent_Keys => "=");
   use SS_Hashed_Maps;

   procedure addhashkey (amap   : in out SS_Hashed_Maps.Map;
                         akey   : in Unbounded_String;
                         avalue : in Unbounded_String) is
   begin
      --  Looks good
      amap.Include (TS (akey), TS (avalue));
   end addhashkey;

   function runcommand (Command_Unbound : in Unbounded_String)
     return Unbounded_String is
      Command  : constant String := TS (Command_Unbound);
      Pipe    : aliased Util.Streams.Pipes.Pipe_Stream;
      Buffer  : Util.Streams.Buffered.Input_Buffer_Stream;
      Content : Unbounded_String;
   begin
      Pipe.Open (Command, Util.Processes.READ);
      Buffer.Initialize (Pipe'Access, 1024);
      Buffer.Read (Content);
      Pipe.Close;
      if Pipe.Get_Exit_Status /= 0 then
         Put_Line (Command & " exited with status " & Integer'Image (Pipe.Get_Exit_Status));
         return (TUS (""));
      end if;
      return (Content);
   end runcommand;

   function runcommand (Command : in String) return Unbounded_String is
   begin return runcommand (TUS (Command)); end runcommand;

   procedure S_splitcreate (Sliceset : in out Slice_Set;
                            From : in String;
                            Separators : in String) is
   begin
      String_Split.Create (S          => Sliceset,
                           From       => From,
                           Separators => Separators,
                           Mode       => String_Split.Multiple);
   end S_splitcreate;

begin
   declare
      star_string : constant String := TS (65 * "*");
      plus_string : constant String := TS (65 * "+");
      head_string : constant String
        := "|    NAME      | SIZE  | RO  |      TYPE     |    MOUNTPOINT     |";
      Glabel_Output : Unbounded_String;
      Mount_Output : Unbounded_String;
      Pgrep_Output : Unbounded_String;
      Gpart_Output : Unbounded_String;
      nam2dev   : SS_Hashed_Maps.Map;
      dev2nam   : SS_Hashed_Maps.Map;
      dev2mount : SS_Hashed_Maps.Map;
      dev2ro    : SS_Hashed_Maps.Map;
      Lines : String_Split.Slice_Set;
      MYLF : constant String := "" & Latin_1.LF;
      J : String_Split.Slice_Number := 0;
   begin

      --  EXECUTE_GLABEL
      Glabel_Output := runcommand ("glabel status | awk '{if (NR!=1) print $1,$3}'");
      --  GLABELOUTPUT_SPLIT
      S_splitcreate (Sliceset => Lines,
                     From     => TS (Glabel_Output),
                     Separators => MYLF & Latin_1.HT);
      J := String_Split.Slice_Count (Lines) - 1;
      for I in 1 .. J loop
         declare
            Line : constant String := String_Split.Slice (Lines, I);
            Words : String_Split.Slice_Set;
            Devname : Unbounded_String;
            Partname : Unbounded_String;
         begin
            S_splitcreate (Sliceset   => Words,
                           From       => Line,
                           Separators => " " & Latin_1.HT);
            Partname := "/dev/" & TUS (String_Split.Slice (Words, 1));
            Devname := "/dev/" & TUS (String_Split.Slice (Words, 2));
            addhashkey (dev2nam, Devname, Partname);
            addhashkey (nam2dev, Partname, Devname);
         end;
      end loop;

      --  EXECUTE_MOUNT
      Mount_Output := runcommand ("mount | grep -e ^/dev/ ");
      --  MOUNTOUTPUT_SPLIT
      S_splitcreate (Sliceset   => Lines,
                     From       => TS (Mount_Output),
                     Separators => MYLF & Latin_1.HT);
      J := String_Split.Slice_Count (Lines) - 1;
      for I in 1 .. J loop
         declare
            Line   : constant String := String_Split.Slice (Lines, I);
            Words  : String_Split.Slice_Set;
            Device : Unbounded_String;
            Path   : Unbounded_String;
            Compiled_Expression : constant Pat.Pattern_Matcher := Pat.Compile ("read-only");
            Option : Unbounded_String;
         begin
            S_splitcreate (Sliceset   => Words,
                           From       => Line,
                           Separators => " " & Latin_1.HT);
            Device := TUS (String_Split.Slice (Words, 1));
            if nam2dev.Contains (TS (Device)) then
               Device := TUS (nam2dev (TS (Device)));
            end if;
            Path := TUS (String_Split.Slice (Words, 3));
            addhashkey (dev2mount, Device, Path);
            if Pat.Match (Compiled_Expression, Line) then
               Option := TUS ("ro");
            else
               Option := TUS ("rw");
            end if;
            addhashkey (dev2ro, Device, Option);
         end;
      end loop;

      --  EXECUTE_PGREP
      --  Vermadensline
      Pgrep_Output := runcommand ("pgrep ntfs-3g");
      --  PGREPOUTPUT_SPLIT
      S_splitcreate (Sliceset   => Lines,
                     From       => TS (Pgrep_Output),
                     Separators => MYLF & Latin_1.HT);
      J := String_Split.Slice_Count (Lines) - 1;
      for I in 1 .. J loop
         declare
            Line   : constant String := String_Split.Slice (Lines, I);
            Words  : String_Split.Slice_Set;
            Ps_Command : Unbounded_String;
            Ps_Output : Unbounded_String;
            Device : Unbounded_String;
            Mount : Unbounded_String;
            Mylen   : Integer;
         begin
            --  Vermadensline
            Ps_Command := TUS ("ps -p " & Line & " -o command | sed 1d | sort -u");
            Ps_Output := runcommand (Ps_Command);
            S_splitcreate (Sliceset   => Words,
                           From       => TS (Ps_Output),
                           Separators => " " & Latin_1.HT);
            Device := TUS (String_Split.Slice (Words, 4));
            Mount := TUS (String_Split.Slice (Words, 5));
            Mylen := Ada.Strings.Unbounded.Length (Mount);
            Mount := Ada.Strings.Unbounded.Delete (Mount, Mylen, Mylen);
            addhashkey (dev2mount, Device, Mount);
            addhashkey (dev2ro, Device, TUS ("?"));
         end;
      end loop;

      --  EXECUTE_GPART
      Gpart_Output := runcommand
        (TUS ("gpart show -p"));
      --  GPART_SPLIT
      S_splitcreate (Sliceset   => Lines,
                     From       => TS (Gpart_Output),
                     Separators => MYLF & Latin_1.HT);
      J := String_Split.Slice_Count (Lines) - 1;
      Put_Line (head_string);
      for I in 1 .. J loop
         declare
            Line   : constant String := String_Split.Slice (Lines, I);
            ULine  : Unbounded_String;
            Words  : String_Split.Slice_Set;
            Mychar : Character;
            dtemp  : Unbounded_String;
            aname  : Unbounded_String;
            amount : Unbounded_String;
            atype  : Unbounded_String;
            ro     : Unbounded_String;
            asize  : Unbounded_String;
            output : Unbounded_String;
            posb : Integer;
            pose : Integer;
         begin
            Mychar := Ada.Strings.Unbounded.Element (TUS (Line), 1);
            ULine := TUS (Line);
            if Mychar = '=' then
               Put_Line (star_string);
               ULine := Ada.Strings.Unbounded.Delete (ULine, 1, 2);
            end if;
            S_splitcreate (Sliceset   => Words,
                           From       => Line,
                           Separators => " " & Latin_1.HT);
            dtemp := TUS (String_Split.Slice (Words, 4));
            if dtemp = "-" then
               aname := TUS ("-");
            else
               aname := "/dev/" & dtemp;
            end if;
            amount := TUS ("-");
            if dev2mount.Contains (TS (aname)) then
               amount := TUS (dev2mount (TS (aname)));
            end if;
            atype := TUS (String_Split.Slice (Words, 5));
            if dev2ro.Contains (TS (aname)) then
               ro := TUS (dev2ro (TS (aname)));
            else
               ro := TUS ("-");
            end if;
            posb := Pat.Match ("\(", TS (ULine));
            pose := Pat.Match ("\)", TS (ULine));
            asize := TUS (Ada.Strings.Unbounded.Slice (ULine, posb + 1, pose - 1));
            aname :=  Padme (aname, 14);
            asize :=  Padme (asize, 6);
            ro    :=  Padme (ro, 4);
            atype :=  Padme (atype, 15);
            amount := Padme (amount, 20);
            output := aname & asize & ro & atype & amount & "|";
            Put_Line (output);
            if Mychar = '=' then
               Put_Line (plus_string);
            end if;
         end;
      end loop;
   end;
end lsblk; 
